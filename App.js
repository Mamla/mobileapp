import 'react-native-gesture-handler';
import React from 'react';
// import HomePage from './app/src/HomePage/HomePage';
// import LoginPage from './app/src/LoginPage/LoginPage';
import AppNavigator from './app/src/AppNavigator/AppNavigator';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import {compose, applyMiddleware, createStore} from 'redux';
import rootReducer from './app/store/reducers/rootReducer';
import FlashMessage from "react-native-flash-message";

const middleware = [thunk];
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(...middleware)));

const App = () => {
  return (
    <Provider store={store}>
      <AppNavigator />
      <FlashMessage position="top" />
    </Provider>
  );
};

export default App;
