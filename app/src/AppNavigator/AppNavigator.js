import React from "react";
import { useSelector } from "react-redux";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import HomePage from '../HomePage/HomePage';
import WelcomePage from '../WelcomePage/WelcomePage';
import LoginPage from '../LoginPage/LoginPage';
import SignupPage from '../SignupPage/SignupPage';
import SignoutPage from '../SignoutPage/SignoutPage';
import UsersPage from '../UsersPage/UsersPage';
import CreateUserPage from '../CreateUserPage/CreateUserPage';
import EditUserPage from '../EditUserPage/EditUserPage';
import ProductsPage from '../ProductsPage/ProductsPage';
import CreateProductPage from '../CreateProductPage/CreateProductPage';
import EditProductPage from '../EditProductPage/EditProductPage';
import TasksPage from '../TasksPage/TasksPage';
import CreateTaskPage from '../CreateTaskPage/CreateTaskPage';
import EditTaskPage from '../EditTaskPage/EditTaskPage';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const HomeNavigator = () => {
  const isSignedIn = useSelector(state => state.auth.isSignedIn);

  return (
    <Stack.Navigator>
      {isSignedIn ? (
        <>
           <Stack.Screen name="Welcome" component={WelcomePage} />
           <Stack.Screen name="CreateUser" component={CreateUserPage} />
           <Stack.Screen name="EditUser" component={EditUserPage} />
           <Stack.Screen name="CreateProduct" component={CreateProductPage} />
           <Stack.Screen name="EditProduct" component={EditProductPage} />
           <Stack.Screen name="Tasks" component={TasksPage} />
           <Stack.Screen name="CreateTask" component={CreateTaskPage} />
           <Stack.Screen name="EditTask" component={EditTaskPage} />
        </>
      ) : (
        <>
          <Stack.Screen name="Home" component={HomePage} />
          <Stack.Screen name="Login" component={LoginPage} />
          <Stack.Screen name="Signup" component={SignupPage} />
        </>
      )}
    </Stack.Navigator>
  );
};

const SignoutNavigator = () => {
  const isSignedIn = useSelector(state => state.auth.isSignedIn);

  return (
    <Stack.Navigator>
        <Stack.Screen name="Signout" component={SignoutPage} />
    </Stack.Navigator>
  );
}

const AppNavigator = () => {
  const isSignedIn = useSelector(state => state.auth.isSignedIn);

  return (
      <NavigationContainer>
        <Drawer.Navigator>
             <Drawer.Screen name="Home" component={HomeNavigator} />
             {isSignedIn ? (
                <>
                  <Drawer.Screen name="Users" component={UsersPage} />
                  <Drawer.Screen name="Products" component={ProductsPage} />
                  <Drawer.Screen name="Sign out" component={SignoutNavigator} />
                </>
              ) : null}
        </Drawer.Navigator>
      </NavigationContainer>
  );
};

export default AppNavigator;


