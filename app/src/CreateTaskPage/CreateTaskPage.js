import React, { Component, useState, useEffect }  from "react";
import { StyleSheet, Text, TextInput, View, Picker } from "react-native";
import Button from "react-native-button";
import { AppStyles } from "../../AppStyles";
import { useDispatch, useSelector } from 'react-redux';
import { createTask } from '../../store/actions/tasksActions';
import { useNavigation } from '@react-navigation/native';
import { getUsers } from '../../store/actions/usersActions';

const CreateTaskPage = ({ route, navigation }) => {
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [userId, setUserId] = useState(6);
  const [productId, setProductId] = useState(route.params.product_id);
  const users = useSelector(state => state.users);
  const token = useSelector(state => state.auth.token);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUsers(token));
  }, []);

  useEffect(() => {
    setProductId(route.params.product_id);
  }, [route]);

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(createTask({title: title, description: description, user_id: userId, product_id: productId }, token, navigation));
  }

  const pickerItems = users.map((user) =>
    <Picker.Item key={user.id} label={user.attributes.email} value={user.id} />
  );

  return (
    <View style={styles.container}>
      <Text style={[styles.title, styles.leftTitle]}>Create new task</Text>
      <View style={styles.InputContainer}>
        <TextInput
          style={styles.body}
          placeholder="Title"
          onChangeText={text => setTitle(text)}
          value={title}
          placeholderTextColor={AppStyles.color.grey}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
      </View>
      <View style={styles.InputContainer}>
        <TextInput
          style={styles.body}
          placeholder="Description"
          onChangeText={text => setDescription(text)}
          value={description}
          placeholderTextColor={AppStyles.color.grey}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
      </View>
       <Text style={[styles.title, styles.roleTitle]}>Choose User</Text>
       <Picker
        selectedValue={userId}
        style={{ height: 20, width: 150, marginTop: -100 }}
        onValueChange={(itemValue, itemIndex) => setUserId(itemValue)}
      >
        {pickerItems}
      </Picker>
      <Button
        containerStyle={[styles.facebookContainer]}
        style={styles.facebookText}
        onPress={handleSubmit}
      >
        Create
      </Button>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center"
  },
  title: {
    fontSize: AppStyles.fontSize.title,
    fontWeight: "bold",
    color: AppStyles.color.tint,
    marginTop: 20,
    marginBottom: 20
  },
  leftTitle: {
    alignSelf: "stretch",
    textAlign: "left",
    marginLeft: 20
  },
  roleTitle: {
    marginTop: 0,
    padding: 50
  },
  content: {
    paddingLeft: 50,
    paddingRight: 50,
    textAlign: "center",
    fontSize: AppStyles.fontSize.content,
    color: AppStyles.color.text
  },
  loginContainer: {
    width: AppStyles.buttonWidth.main,
    backgroundColor: AppStyles.color.tint,
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    marginTop: 30
  },
  loginText: {
    color: AppStyles.color.white
  },
  placeholder: {
    fontFamily: AppStyles.fontName.text,
    color: "red"
  },
  InputContainer: {
    width: AppStyles.textInputWidth.main,
    marginTop: 30,
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: AppStyles.color.grey,
    borderRadius: AppStyles.borderRadius.main
  },
  body: {
    height: 42,
    paddingLeft: 20,
    paddingRight: 20,
    color: AppStyles.color.text
  },
  facebookContainer: {
    width: AppStyles.buttonWidth.main,
    backgroundColor: AppStyles.color.tint,
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    marginTop: 200
  },
  facebookText: {
    color: AppStyles.color.white
  }
});

export default CreateTaskPage;
