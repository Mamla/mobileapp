import React, { Component, useState, useEffect }  from "react";
import { StyleSheet, Text, TextInput, View, Picker } from "react-native";
import Button from "react-native-button";
import { AppStyles } from "../../AppStyles";
import { useDispatch, useSelector } from 'react-redux';
import { createProduct } from '../../store/actions/productsActions';
import { useNavigation } from '@react-navigation/native';

const CreateProductPage = () => {
  const [title, setTitle] = useState('');
  const token = useSelector(state => state.auth.token);
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(createProduct({title: title }, token, navigation));
  }

  return (
    <View style={styles.container}>
      <Text style={[styles.title, styles.leftTitle]}>Create new product</Text>
      <View style={styles.InputContainer}>
        <TextInput
          style={styles.body}
          placeholder="Title"
          onChangeText={text => setTitle(text)}
          value={title}
          placeholderTextColor={AppStyles.color.grey}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
      </View>
      <Button
        containerStyle={[styles.facebookContainer]}
        style={styles.facebookText}
        onPress={handleSubmit}
      >
        Create
      </Button>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center"
  },
  title: {
    fontSize: AppStyles.fontSize.title,
    fontWeight: "bold",
    color: AppStyles.color.tint,
    marginTop: 20,
    marginBottom: 20
  },
  leftTitle: {
    alignSelf: "stretch",
    textAlign: "left",
    marginLeft: 20
  },
  roleTitle: {
    marginTop: 0,
    padding: 50
  },
  content: {
    paddingLeft: 50,
    paddingRight: 50,
    textAlign: "center",
    fontSize: AppStyles.fontSize.content,
    color: AppStyles.color.text
  },
  loginContainer: {
    width: AppStyles.buttonWidth.main,
    backgroundColor: AppStyles.color.tint,
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    marginTop: 30
  },
  loginText: {
    color: AppStyles.color.white
  },
  placeholder: {
    fontFamily: AppStyles.fontName.text,
    color: "red"
  },
  InputContainer: {
    width: AppStyles.textInputWidth.main,
    marginTop: 30,
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: AppStyles.color.grey,
    borderRadius: AppStyles.borderRadius.main
  },
  body: {
    height: 42,
    paddingLeft: 20,
    paddingRight: 20,
    color: AppStyles.color.text
  },
  facebookContainer: {
    width: AppStyles.buttonWidth.main,
    backgroundColor: AppStyles.color.tint,
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    marginTop: 30
  },
  facebookText: {
    color: AppStyles.color.white
  }
});

export default CreateProductPage;
