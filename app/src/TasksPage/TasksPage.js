import React, { useEffect, useState } from "react";
import Button from "react-native-button";
import { AppStyles } from "../../AppStyles";
import { useDispatch, useSelector } from "react-redux";
import { getTasks } from '../../store/actions/tasksActions';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import { Table, TableWrapper, Row } from 'react-native-table-component';

const TasksPage = ({route, navigation}) => {
  const dispatch = useDispatch();
  const token = useSelector(state => state.auth.token);
  const tasks = useSelector(state => state.tasks);

  useEffect(() => {
    dispatch(getTasks(token));
  }, []);

  return (
    <View style={styles.container}>
      <Button
        containerStyle={[styles.facebookContainer, { marginTop: 50 }]}
        style={styles.facebookText}
        onPress={() => navigation.navigate('CreateTask', { product_id: route.params.product_id })}
      >
        Add new task

      </Button>
      <Text style={styles.title}>Tasks</Text>
      <FlatList
        data={tasks}
        renderItem={({item}) => <Text onPress={() => {
                      navigation.navigate('EditTask', item);
                    }} style={styles.item}>{item.attributes.title}</Text>}
      />
      </View>
  );
}

const styles = StyleSheet.create({
  container: { flex: 1, padding: 16, paddingTop: 60, backgroundColor: '#fff' },
    title: {
    fontSize: AppStyles.fontSize.title,
    fontWeight: "bold",
    color: AppStyles.color.tint,
    marginTop: 20,
    textAlign: "center",
    marginBottom: 20,
    marginLeft: 20,
    marginRight: 20
  },
  head: {
    height: 50,
    backgroundColor: '#6F7BD9'
  },
  text: {
    textAlign: 'center',
    fontWeight: '200'
  },
  dataWrapper: {
    marginTop: -1
  },
  row: {
    height: 40,
    backgroundColor: '#F7F8FA'
  },
  body: {
    height: 42,
    paddingLeft: 20,
    paddingRight: 20,
    color: AppStyles.color.text
  },
  facebookContainer: {
    width: AppStyles.buttonWidth.main,
    backgroundColor: AppStyles.color.tint,
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    marginTop: 30,
    marginLeft: 60
  },
  facebookText: {
    color: AppStyles.color.white
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  }
});

export default TasksPage;
