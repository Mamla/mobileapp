import React, { Component, useState }  from "react";
import { StyleSheet, Text, TextInput, View, Picker } from "react-native";
import Button from "react-native-button";
import { AppStyles } from "../../AppStyles";
import { useDispatch, useSelector } from 'react-redux';
import { createUser } from '../../store/actions/usersActions';
import { useNavigation } from '@react-navigation/native';

const CreateUserPage = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [passwordConfirmation, setPasswordConfirmation] = useState('');
  const [name, setName] = useState('');
  const [role, setRole] = useState('client');
  const token = useSelector(state => state.auth.token);
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(createUser({email: email, password: password, password_confirmation: passwordConfirmation, name: name, role: role}, token, navigation));
  }

  return (
    <View style={styles.container}>
      <Text style={[styles.title, styles.leftTitle]}>Create new user</Text>
      <View style={styles.InputContainer}>
        <TextInput
          style={styles.body}
          placeholder="Name"
          onChangeText={text => setName(text)}
          value={name}
          placeholderTextColor={AppStyles.color.grey}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
      </View>
      <View style={styles.InputContainer}>
        <TextInput
          style={styles.body}
          placeholder="E-mail Address"
          onChangeText={text => setEmail(text)}
          value={email}
          placeholderTextColor={AppStyles.color.grey}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
      </View>
      <View style={styles.InputContainer}>
        <TextInput
          style={styles.body}
          placeholder="Password"
          secureTextEntry={true}
          onChangeText={text => setPassword(text)}
          value={password}
          placeholderTextColor={AppStyles.color.grey}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
      </View>
      <View style={styles.InputContainer}>
        <TextInput
          style={styles.body}
          placeholder="Password Confirmation"
          secureTextEntry={true}
          onChangeText={text => setPasswordConfirmation(text)}
          value={passwordConfirmation}
          placeholderTextColor={AppStyles.color.grey}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
      </View>
       <Text style={[styles.title, styles.roleTitle]}>Choose Role</Text>
       <Picker
        selectedValue={role}
        style={{ height: 20, width: 150, marginTop: -100 }}
        onValueChange={(itemValue, itemIndex) => setRole(itemValue)}
      >
        <Picker.Item label="Client" value="client" />
        <Picker.Item label="Employee" value="employee" />
        <Picker.Item label="Admin" value="admin" />
      </Picker>
      <Button
        containerStyle={[styles.facebookContainer, { marginTop: 150 }]}
        style={styles.facebookText}
        onPress={handleSubmit}
      >
        Create
      </Button>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center"
  },
  title: {
    fontSize: AppStyles.fontSize.title,
    fontWeight: "bold",
    color: AppStyles.color.tint,
    marginTop: 20,
    marginBottom: 20
  },
  leftTitle: {
    alignSelf: "stretch",
    textAlign: "left",
    marginLeft: 20
  },
  roleTitle: {
    marginTop: 0,
    padding: 50
  },
  content: {
    paddingLeft: 50,
    paddingRight: 50,
    textAlign: "center",
    fontSize: AppStyles.fontSize.content,
    color: AppStyles.color.text
  },
  loginContainer: {
    width: AppStyles.buttonWidth.main,
    backgroundColor: AppStyles.color.tint,
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    marginTop: 30
  },
  loginText: {
    color: AppStyles.color.white
  },
  placeholder: {
    fontFamily: AppStyles.fontName.text,
    color: "red"
  },
  InputContainer: {
    width: AppStyles.textInputWidth.main,
    marginTop: 30,
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: AppStyles.color.grey,
    borderRadius: AppStyles.borderRadius.main
  },
  body: {
    height: 42,
    paddingLeft: 20,
    paddingRight: 20,
    color: AppStyles.color.text
  },
  facebookContainer: {
    width: AppStyles.buttonWidth.main,
    backgroundColor: AppStyles.color.tint,
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    marginTop: 30
  },
  facebookText: {
    color: AppStyles.color.white
  }
});

export default CreateUserPage;
