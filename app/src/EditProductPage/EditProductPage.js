import React, { Component, useState, useEffect }  from "react";
import { StyleSheet, Text, TextInput, View, Picker, Alert } from "react-native";
import Button from "react-native-button";
import { AppStyles } from "../../AppStyles";
import { useDispatch, useSelector } from 'react-redux';
import { updateProduct, deleteProduct } from '../../store/actions/productsActions';
import { useNavigation } from '@react-navigation/native';

const EditProductPage = ({ route, navigation }) => {
  const [id, setId] = useState(route.params.id);
  const [title, setTitle] = useState(route.params.attributes.title);

  useEffect(() => {
    setId(route.params.id);
    setTitle(route.params.attributes.title);
  }, [route]);

  const token = useSelector(state => state.auth.token);
  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(updateProduct({id: id, title: title}, token, navigation));
  }

  const handleDelete = (e) => {
    e.preventDefault();

    Alert.alert(
      "Warning",
      "Are you sure?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => dispatch(deleteProduct(id, token, navigation)) }
      ],
      { cancelable: false }
    );
  }

  return (
    <View style={styles.container}>
      <Text style={[styles.title, styles.leftTitle]}>Edit a product</Text>
      <View style={styles.InputContainer}>
        <TextInput
          style={styles.body}
          placeholder="Title"
          onChangeText={text => setTitle(text)}
          value={title}
          placeholderTextColor={AppStyles.color.grey}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
      </View>
      <Button
        containerStyle={[styles.facebookContainer, { marginTop: 5 }]}
        style={styles.facebookText}
        onPress={handleSubmit}
      >
        Update
      </Button>
      <Button
        containerStyle={[styles.facebookContainer, { backgroundColor: "#f0a224", marginTop: 1 }]}
        style={styles.facebookText}
        onPress={handleDelete}
      >
        Remove Product
      </Button>
      <Button
        containerStyle={[styles.facebookContainer, { backgroundColor: "#326fd1", marginTop: 1 }]}
        style={styles.facebookText}
        onPress={() => { navigation.navigate('Tasks', { product_id: id}); }}
      >
        Show Tasks
      </Button>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center"
  },
  title: {
    fontSize: AppStyles.fontSize.title,
    fontWeight: "bold",
    color: AppStyles.color.tint,
    marginTop: 20,
    marginBottom: 20
  },
  leftTitle: {
    alignSelf: "stretch",
    textAlign: "left",
    marginLeft: 20
  },
  roleTitle: {
    marginTop: 0,
    padding: 50
  },
  content: {
    paddingLeft: 50,
    paddingRight: 50,
    textAlign: "center",
    fontSize: AppStyles.fontSize.content,
    color: AppStyles.color.text
  },
  loginContainer: {
    width: AppStyles.buttonWidth.main,
    backgroundColor: AppStyles.color.tint,
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    marginTop: 30
  },
  loginText: {
    color: AppStyles.color.white
  },
  placeholder: {
    fontFamily: AppStyles.fontName.text,
    color: "red"
  },
  InputContainer: {
    width: AppStyles.textInputWidth.main,
    marginTop: 30,
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: AppStyles.color.grey,
    borderRadius: AppStyles.borderRadius.main
  },
  body: {
    height: 42,
    paddingLeft: 20,
    paddingRight: 20,
    color: AppStyles.color.text
  },
  facebookContainer: {
    width: AppStyles.buttonWidth.main,
    backgroundColor: AppStyles.color.tint,
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    marginTop: 30
  },
  facebookText: {
    color: AppStyles.color.white
  }
});

export default EditProductPage;
