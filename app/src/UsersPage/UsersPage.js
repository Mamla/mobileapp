import React, { useEffect } from "react";
import Button from "react-native-button";
import { AppStyles } from "../../AppStyles";
import { useDispatch, useSelector } from "react-redux";
import { getUsers } from '../../store/actions/usersActions';
import { StyleSheet, View, ScrollView, Text } from 'react-native';
import { Table, TableWrapper, Row } from 'react-native-table-component';

const UsersPage = ({navigation}) => {
  const dispatch = useDispatch();
  const token = useSelector(state => state.auth.token);
  const users = useSelector(state => state.users);

  useEffect(() => {
    dispatch(getUsers(token));
  }, []);

  const tableHead = ['Id', 'Email', 'Name', 'Role'];
  const widthArr = [40, 200, 100, 70];
  const tableData = [];

  for (let i = 0; i < users.length; i += 1) {
    const rowData = [];
    let user = users[i];
    rowData.push(user.id);
    rowData.push(user.attributes.email);
    rowData.push(user.attributes.name);
    rowData.push(user.attributes.role);

    tableData.push(rowData);
  }
  return (
    <View style={styles.container}>
      <Button
        containerStyle={[styles.facebookContainer, { marginTop: 50 }]}
        style={styles.facebookText}
        onPress={() => navigation.navigate('CreateUser')}
      >
        Add new user
      </Button>
      <Text style={styles.title}>Users</Text>
      <ScrollView horizontal={true}>
        <View>
          <Table borderStyle={{borderColor: '#C1C0B9'}}>
            <Row data={tableHead} widthArr={widthArr} style={styles.header} textStyle={styles.text}/>
          </Table>
          <ScrollView style={styles.dataWrapper}>
            <Table borderStyle={{borderColor: '#C1C0B9'}}>
              {
                tableData.map((rowData, index) => (
                  <Row
                    key={index}
                    data={rowData}
                    widthArr={widthArr}
                    style={[styles.row, index%2 && {backgroundColor: '#F7F6E7'}]}
                    textStyle={styles.text}
                    onPress={() => {
                      navigation.navigate('EditUser', rowData);
                    }}
                  />
                ))
              }
            </Table>
          </ScrollView>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: { flex: 1, padding: 16, paddingTop: 60, backgroundColor: '#fff' },
  title: {
    fontSize: AppStyles.fontSize.title,
    fontWeight: "bold",
    color: AppStyles.color.tint,
    marginTop: 20,
    textAlign: "center",
    marginBottom: 20,
    marginLeft: 20,
    marginRight: 20
  },
  head: {
    height: 50,
    backgroundColor: '#6F7BD9'
  },
  text: {
    textAlign: 'center',
    fontWeight: '200'
  },
  dataWrapper: {
    marginTop: -1
  },
  row: {
    height: 40,
    backgroundColor: '#F7F8FA'
  },
  body: {
    height: 42,
    paddingLeft: 20,
    paddingRight: 20,
    color: AppStyles.color.text
  },
  facebookContainer: {
    width: AppStyles.buttonWidth.main,
    backgroundColor: AppStyles.color.tint,
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    marginTop: 30,
    marginLeft: 60
  },
  facebookText: {
    color: AppStyles.color.white
  }
});

export default UsersPage;
