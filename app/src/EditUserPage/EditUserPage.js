import React, { Component, useState, useEffect }  from "react";
import { StyleSheet, Text, TextInput, View, Picker, Alert } from "react-native";
import Button from "react-native-button";
import { AppStyles } from "../../AppStyles";
import { useDispatch, useSelector } from 'react-redux';
import { updateUser, deleteUser } from '../../store/actions/usersActions';
import { useNavigation } from '@react-navigation/native';

const EditUserPage = ({ route, navigation }) => {
  const [id, setId] = useState(route.params[0]);
  const [email, setEmail] = useState(route.params[1]);
  const [name, setName] = useState(route.params[2]);
  const [role, setRole] = useState(route.params[3]);
  const token = useSelector(state => state.auth.token);
  const dispatch = useDispatch();

  useEffect(() => {
    setId(route.params[0]);
    setEmail(route.params[1]);
    setName(route.params[2]);
    setRole(route.params[3]);
  }, [route]);

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(updateUser({id: id, email: email, name: name, role: role}, token, navigation));
  }

  const handleDelete = (e) => {
    e.preventDefault();

    Alert.alert(
      "Warning",
      "Are you sure?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => dispatch(deleteUser(id, token, navigation)) }
      ],
      { cancelable: false }
    );
  }

  return (
    <View style={styles.container}>
      <Text style={[styles.title, styles.leftTitle]}>Edit a user</Text>
      <View style={styles.InputContainer}>
        <TextInput
          style={styles.body}
          placeholder="Name"
          onChangeText={text => setName(text)}
          value={name}
          placeholderTextColor={AppStyles.color.grey}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
      </View>
      <View style={styles.InputContainer}>
        <TextInput
          style={styles.body}
          placeholder="E-mail Address"
          onChangeText={text => setEmail(text)}
          value={email}
          placeholderTextColor={AppStyles.color.grey}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
      </View>
       <Text style={[styles.title, styles.roleTitle]}>Choose Role</Text>
       <Picker
        selectedValue={role}
        style={{ height: 20, width: 150, marginTop: -100 }}
        onValueChange={(itemValue, itemIndex) => setRole(itemValue)}
      >
        <Picker.Item label="Client" value="client" />
        <Picker.Item label="Employee" value="employee" />
        <Picker.Item label="Admin" value="admin" />
      </Picker>
      <Button
        containerStyle={[styles.facebookContainer, { marginTop: 150 }]}
        style={styles.facebookText}
        onPress={handleSubmit}
      >
        Update
      </Button>
      <Button
        containerStyle={[styles.facebookContainer, { backgroundColor: "#f0a224", marginTop: 1 }]}
        style={styles.facebookText}
        onPress={handleDelete}
      >
        Remove User
      </Button>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center"
  },
  title: {
    fontSize: AppStyles.fontSize.title,
    fontWeight: "bold",
    color: AppStyles.color.tint,
    marginTop: 20,
    marginBottom: 20
  },
  leftTitle: {
    alignSelf: "stretch",
    textAlign: "left",
    marginLeft: 20
  },
  roleTitle: {
    marginTop: 0,
    padding: 50
  },
  content: {
    paddingLeft: 50,
    paddingRight: 50,
    textAlign: "center",
    fontSize: AppStyles.fontSize.content,
    color: AppStyles.color.text
  },
  loginContainer: {
    width: AppStyles.buttonWidth.main,
    backgroundColor: AppStyles.color.tint,
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    marginTop: 30
  },
  loginText: {
    color: AppStyles.color.white
  },
  placeholder: {
    fontFamily: AppStyles.fontName.text,
    color: "red"
  },
  InputContainer: {
    width: AppStyles.textInputWidth.main,
    marginTop: 30,
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: AppStyles.color.grey,
    borderRadius: AppStyles.borderRadius.main
  },
  body: {
    height: 42,
    paddingLeft: 20,
    paddingRight: 20,
    color: AppStyles.color.text
  },
  facebookContainer: {
    width: AppStyles.buttonWidth.main,
    backgroundColor: AppStyles.color.tint,
    borderRadius: AppStyles.borderRadius.main,
    padding: 10,
    marginTop: 30
  },
  facebookText: {
    color: AppStyles.color.white
  }
});

export default EditUserPage;
