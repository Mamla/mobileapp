import { LOAD_TASKS, ADD_TASK_SUCCESS, ADD_TASK_ERROR, DELETE_TASK,
   UPDATE_TASK_SUCCESS, UPDATE_TASK_ERROR }  from '../actionTypes/tasks';

const tasksReducer = (state = [], action) => {
    switch(action.type) {
        case LOAD_TASKS:
            return action.tasks;
        case ADD_TASK_SUCCESS:
            return state.concat(action.task);
        case ADD_TASK_ERROR:
            return {
                ...state
            };
        case UPDATE_TASK_SUCCESS:
            return state.map(task => (task.id === action.task.id)
                      ? {...task, ...action.task }
                      : task
            );
        case UPDATE_TASK_ERROR:
            return {
                ...state
            };
        case DELETE_TASK:
            return state.filter(task => task.id !== action.id);
        default:
            return state;
    }
}

export default tasksReducer;
