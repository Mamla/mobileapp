import { LOAD_PRODUCTS, ADD_PRODUCT_SUCCESS, ADD_PRODUCT_ERROR, DELETE_PRODUCT,
   UPDATE_PRODUCT_SUCCESS, UPDATE_PRODUCT_ERROR }  from '../actionTypes/products';

const productsReducer = (state = [], action) => {
    switch(action.type) {
        case LOAD_PRODUCTS:
            return action.products;
        case ADD_PRODUCT_SUCCESS:
            return state.concat(action.product);
        case ADD_PRODUCT_ERROR:
            return {
                ...state
            };
        case UPDATE_PRODUCT_SUCCESS:
            return state.map(product => (product.id === action.product.id)
                      ? {...product, ...action.product }
                      : product
            );
        case UPDATE_PRODUCT_ERROR:
            return {
                ...state
            };
        case DELETE_PRODUCT:
            return state.filter(product => product.id !== action.id);
        default:
            return state;
    }
}

export default productsReducer;
