import { LOAD_USERS, ADD_USER_SUCCESS, ADD_USER_ERROR, DELETE_USER,
   UPDATE_USER_SUCCESS, UPDATE_USER_ERROR }  from '../actionTypes/users';

const usersReducer = (state = [], action) => {
    switch(action.type) {
        case LOAD_USERS:
            return action.users;
        case ADD_USER_SUCCESS:
            return state.concat(action.user);
        case ADD_USER_ERROR:
            return {
                ...state
            };
        case UPDATE_USER_SUCCESS:
            return state.map(user => (user.id === action.user.id)
                      ? {...user, ...action.user }
                      : user
            );
        case UPDATE_USER_ERROR:
            return {
                ...state
            };
        case DELETE_USER:
            return state.filter(user => user.id !== action.id);
        default:
            return state;
    }
}

export default usersReducer;
