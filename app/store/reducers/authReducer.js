import { LOGIN_ERROR,
         LOGIN_SUCCESS,
         SIGNOUT_SUCCESS,
         SIGNUP_SUCCESS,
         SIGNUP_ERROR } from '../actionTypes/auth';

const initState = {
  isSignedIn: false,
  user: {},
  token: null
}

const authReducer = (state = initState, action) => {
  switch (action.type) {
    case LOGIN_ERROR:
      return {
        ...state
    };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isSignedIn: true,
        user: action.user,
        token: action.token
      };
    case SIGNOUT_SUCCESS:
      return {
        ...state,
        isSignedIn: false,
        user: {},
        token: null
      };
    case SIGNUP_SUCCESS:
      return {
        ...state,
        isSignedIn: true,
        user: action.user
      }
    case SIGNUP_ERROR:
      return {
        ...state
      }
    default:
      return state;
  }
}

export default authReducer;
