import authReducer from './authReducer';
import usersReducer from './usersReducer';
import productsReducer from './productsReducer';
import tasksReducer from './tasksReducer';

import { combineReducers } from 'redux';

const rootReducer = combineReducers({
  auth: authReducer,
  users: usersReducer,
  products: productsReducer,
  tasks: tasksReducer
});

export default rootReducer;
