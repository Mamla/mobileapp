import { LOAD_USERS, ADD_USER_SUCCESS, ADD_USER_ERROR, DELETE_USER,
  UPDATE_USER_SUCCESS, UPDATE_USER_ERROR}  from '../actionTypes/users';
import axios from '../../axios-base';
import { showMessage } from "react-native-flash-message";

export const getUsers = (token) => {
  return (dispatch) => {
    axios.get('api/v1/users.json', { headers: {Authorization: token}})
          .then(response => {
            dispatch({ type: LOAD_USERS, users: response.data.data });
            showMessage({
              message: "Users loaded.",
              type: "success",
              icon: "success"
            });
          });
  }
}

export const createUser = (newUser, token, navigation) => {
  return (dispatch) => {
    const user = {
      user: {
        ...newUser
      }
    };
    axios.post('api/v1/users', user, { headers: {Authorization: token}})
          .then(response => {
            dispatch({ type: ADD_USER_SUCCESS, user: response.data.data });
            navigation.navigate('Users');
            showMessage({
              message: "User created successfully.",
              type: "success",
              icon: "success"
            });
          })
          .catch(error => {
            dispatch({ type: ADD_USER_ERROR});
            var messages = "";

            Object.entries(error.response.data.errors).map(([key, value]) => {
              messages += key;
              messages += ' ';
              messages += value;
              messages += "\n";
            });
            showMessage({
                    message: "Creating user failed!",
                    description: messages,
                    type: "danger",
                    icon: "danger",
                    duration: 5000
                  });
          });
  }
}

export const updateUser = (editUser, token, navigation) => {
  return (dispatch) => {
    const user = {
      user: {
        ...editUser
      }
    };
    axios.put('api/v1/users/' + editUser.id, user, { headers: {Authorization: token}})
          .then(response => {
            dispatch({ type: UPDATE_USER_SUCCESS, user: response.data.data });
            navigation.navigate('Users');
            showMessage({
              message: "User updated successfully.",
              type: "success",
              icon: "success"
            });
          })
          .catch(error => {
            dispatch({ type: UPDATE_USER_ERROR});
            var messages = "";

            Object.entries(error.response.data.errors).map(([key, value]) => {
              messages += key;
              messages += ' ';
              messages += value;
              messages += "\n";
            });
            showMessage({
                    message: "Updating user failed!",
                    description: messages,
                    type: "danger",
                    icon: "danger",
                    duration: 5000
                  });
          });
  }
}

export const deleteUser = (id, token, navigation ) => {
  return (dispatch) => {
    axios.delete('api/v1/users/' + id, { headers: {Authorization: token}})
          .then(response => {
            dispatch({ type: DELETE_USER, id: id });
            navigation.navigate('Users');
            showMessage({
              message: "User deleted.",
              type: "success",
              icon: "success"
            });
          });
  }
}
