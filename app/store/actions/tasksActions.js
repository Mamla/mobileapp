import { LOAD_TASKS, ADD_TASK_SUCCESS, ADD_TASK_ERROR, DELETE_TASK,
  UPDATE_TASK_SUCCESS, UPDATE_TASK_ERROR}  from '../actionTypes/tasks';
import axios from '../../axios-base';
import { showMessage } from "react-native-flash-message";

export const getTasks = (token) => {
  return (dispatch) => {
    axios.get('api/v1/tasks.json', { headers: {Authorization: token}})
          .then(response => {
            dispatch({ type: LOAD_TASKS, tasks: response.data.data });
            showMessage({
              message: "Tasks loaded.",
              type: "success",
              icon: "success"
            });
          });
  }
}

export const createTask = (newTask, token, navigation) => {
  return (dispatch) => {
    const task = {
      task: {
        ...newTask
      }
    };
    axios.post('api/v1/tasks', task, { headers: {Authorization: token}})
          .then(response => {
            dispatch({ type: ADD_TASK_SUCCESS, task: response.data.data });
            navigation.navigate('Tasks');
            showMessage({
              message: "Task created successfully.",
              type: "success",
              icon: "success"
            });
          })
          .catch(error => {
            dispatch({ type: ADD_TASK_ERROR});
            var messages = "";

            Object.entries(error.response.data.errors).map(([key, value]) => {
              messages += key;
              messages += ' ';
              messages += value;
              messages += "\n";
            });
            showMessage({
                    message: "Creating task failed!",
                    description: messages,
                    type: "danger",
                    icon: "danger",
                    duration: 5000
                  });
          });
  }
}

export const updateTask = (editTask, token, navigation) => {
  return (dispatch) => {
    const task = {
      task: {
        ...editTask
      }
    };
    axios.put('api/v1/tasks/' + editTask.id, task, { headers: {Authorization: token}})
          .then(response => {
            dispatch({ type: UPDATE_TASK_SUCCESS, task: response.data.data });
            navigation.navigate('Tasks');
            showMessage({
              message: "Task updated successfully.",
              type: "success",
              icon: "success"
            });
          })
          .catch(error => {
            dispatch({ type: UPDATE_TASK_ERROR});
            var messages = "";

            Object.entries(error.response.data.errors).map(([key, value]) => {
              messages += key;
              messages += ' ';
              messages += value;
              messages += "\n";
            });
            showMessage({
                    message: "Updating task failed!",
                    description: messages,
                    type: "danger",
                    icon: "danger",
                    duration: 5000
                  });
          });
  }
}

export const deleteTask = (id, token, navigation ) => {
  return (dispatch) => {
    axios.delete('api/v1/tasks/' + id, { headers: {Authorization: token}})
          .then(response => {
            dispatch({ type: DELETE_TASK, id: id });
            navigation.navigate('Tasks');
            showMessage({
              message: "Task deleted.",
              type: "success",
              icon: "success"
            });
          });
  }
}
