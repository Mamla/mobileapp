import { LOAD_PRODUCTS, ADD_PRODUCT_SUCCESS, ADD_PRODUCT_ERROR, DELETE_PRODUCT,
  UPDATE_PRODUCT_SUCCESS, UPDATE_PRODUCT_ERROR}  from '../actionTypes/products';
import axios from '../../axios-base';
import { showMessage } from "react-native-flash-message";

export const getProducts = (token) => {
  return (dispatch) => {
    axios.get('api/v1/products.json', { headers: {Authorization: token}})
          .then(response => {
            dispatch({ type: LOAD_PRODUCTS, products: response.data.data });
            showMessage({
              message: "Products loaded.",
              type: "success",
              icon: "success"
            });
          });
  }
}

export const createProduct = (newProduct, token, navigation) => {
  return (dispatch) => {
    const product = {
      product: {
        ...newProduct
      }
    };
    axios.post('api/v1/products', product, { headers: {Authorization: token}})
          .then(response => {
            dispatch({ type: ADD_PRODUCT_SUCCESS, product: response.data.data });
            navigation.navigate('Products');
            showMessage({
              message: "Product created successfully.",
              type: "success",
              icon: "success"
            });
          })
          .catch(error => {
            dispatch({ type: ADD_PRODUCT_ERROR});
            var messages = "";

            Object.entries(error.response.data.errors).map(([key, value]) => {
              messages += key;
              messages += ' ';
              messages += value;
              messages += "\n";
            });
            showMessage({
                    message: "Creating product failed!",
                    description: messages,
                    type: "danger",
                    icon: "danger",
                    duration: 5000
                  });
          });
  }
}

export const updateProduct = (editProduct, token, navigation) => {
  return (dispatch) => {
    const product = {
      product: {
        ...editProduct
      }
    };
    axios.put('api/v1/products/' + editProduct.id, product, { headers: {Authorization: token}})
          .then(response => {
            dispatch({ type: UPDATE_PRODUCT_SUCCESS, product: response.data.data });
            navigation.navigate('Products');
            showMessage({
              message: "Product updated successfully.",
              type: "success",
              icon: "success"
            });
          })
          .catch(error => {
            dispatch({ type: UPDATE_PRODUCT_ERROR});
            var messages = "";

            Object.entries(error.response.data.errors).map(([key, value]) => {
              messages += key;
              messages += ' ';
              messages += value;
              messages += "\n";
            });
            showMessage({
                    message: "Updating product failed!",
                    description: messages,
                    type: "danger",
                    icon: "danger",
                    duration: 5000
                  });
          });
  }
}

export const deleteProduct = (id, token, navigation ) => {
  return (dispatch) => {
    axios.delete('api/v1/products/' + id, { headers: {Authorization: token}})
          .then(response => {
            dispatch({ type: DELETE_PRODUCT, id: id });
            navigation.navigate('Products');
            showMessage({
              message: "Product deleted.",
              type: "success",
              icon: "success"
            });
          });
  }
}
