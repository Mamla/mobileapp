import { LOGIN_ERROR,
         LOGIN_SUCCESS,
         SIGNOUT_SUCCESS,
         SIGNUP_SUCCESS,
         SIGNUP_ERROR } from '../actionTypes/auth';
import axios from '../../axios-base';
import { showMessage } from "react-native-flash-message";


export const signIn = (credentials) => {
  return (dispatch) => {
    const authData = {
      user: {
        ...credentials
      }
    };
    axios.post('api/v1/login', authData)
          .then(response => {
            dispatch({ type: LOGIN_SUCCESS, user: response.data, token: response.headers.authorization });
            showMessage({
              message: "Signed in successfully.",
              type: "success",
              icon: "success"
            });
          })
          .catch(error => {
            dispatch({ type: LOGIN_ERROR});
            showMessage({
              message: error.response.data.errors,
              type: "danger",
              icon: "danger"
            });
          });
  }
}

export const signOut = () => {
  return (dispatch) => {
    axios.delete('api/v1/logout').then(() => {
      dispatch({ type: SIGNOUT_SUCCESS });
      showMessage({
        message: "Signed out successfully.",
        type: "success",
        icon: "success"
      });
    })
  }
}

export const signUp = (newUser) => {
  return (dispatch) => {
    const user = {
      user: {
        email: newUser.email,
        name: newUser.name,
        password: newUser.password,
        password_confirmation: newUser.passwordConfirmation
      }
    };

    axios.post('api/v1/signup', user).then((response) => {
      dispatch({ type: SIGNUP_SUCCESS, user: response.data })
      showMessage({
        message: "Welcome! You have signed up successfully.",
        type: "success",
        icon: "success"
      });
    }).catch(error => {
      dispatch({ type: SIGNUP_ERROR });
      var messages = "";

      Object.entries(error.response.data.errors).map(([key, value]) => {
        messages += key;
        messages += ' ';
        messages += value;
        messages += "\n";
      });
      showMessage({
              message: "Sign up failed!",
              description: messages,
              type: "danger",
              icon: "danger",
              duration: 5000
            });
    })
  }
}
